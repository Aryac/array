#ifndef ARRAY_H
#define ARRAYEH_H
#include <iostream>
using namespace std;

class Arrayeh
{
private :
    int len_ ;
    int min_ ;
    int border_ ;
    int num_ ;
    int arr[1000] ;
public:
    Arrayeh();

    void setNum (int ) ;
    void setBorder(int);
    int getNum ();
    int getBorder ();
    void display() ;
    void find_Minmax () ;
    void bubbleSort ();
    void search ();
    void show() ;


};

#endif // ARRAY_H
